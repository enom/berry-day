// Credentials via alias
import { g as env } from 'berry-env'

// Interfaces
import Api from './api'

// Libraries - Google API from global window object(no good es6 module)
const { gapi } = window

const API_KEY = env.api_key
const API_CLIENT = env.client_id
const API_DOCS = ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest']
const API_SCOPE = 'https://www.googleapis.com/auth/calendar.readonly'

// Local state
const state = {
  api: null,
  results: null
}

function AuthResponse () {

}

function AuthApi () {
  const bootable = (executor) => this.boot().then(() => new Promise(executor))

  //
  this.name = 'auth'

  //
  this.results = () => state.results

  //
  this.fetch = () => {
    const executor = (resolve) => {
      resolve(state.results = state.api.isSignedIn.get())
    }

    return bootable(executor)
  }

  //
  this.login = () => {
    const executor = (resolve) => {
      if (state.results === true) return resolve()

      const resolved = () => {
        // Update state
        this.fetch().then(resolve)
      }

      // Log in then fetch() to update state
      state.api.signIn().then(resolved)
    }

    return bootable(executor)
  }

  //
  this.logout = () => {
    const executor = (resolve) => {
      if (state.results === false) return resolve()

      const resolved = () => {
        // Update state
        this.fetch().then(resolve)
      }

      // Log out then fetch() to update state
      state.api.signOut().then(resolved)
    }

    return bootable(executor)
  }

  //
  this.boot = () => {
    if (state.api !== null) return Promise.resolve()

    const config = {
      apiKey: API_KEY,
      clientId: API_CLIENT,
      discoveryDocs: API_DOCS,
      scope: API_SCOPE
    }

    const executor = (resolve) => {
      const configured = () => {
        state.api = gapi.auth2.getAuthInstance()
        state.status = state.api.isSignedIn.get()

        resolve()
      }

      const loaded = () => {
        gapi.client.init(config).then(configured)
      }

      gapi.load('client:auth2', loaded)
    }

    return new Promise(executor)
  }
}

// auth implements Api
AuthApi.prototype = new Api()

export default AuthApi
