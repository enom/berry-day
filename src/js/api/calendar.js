// Credentials via alias
import { g as env } from 'berry-env'

// Interfaces
import Api from './api'
import AuthApi from './auth'
import utils from '../utils'

// Libraries - Google API from global window object(no good es6 module)
const { gapi } = window

const API_KEY = env.api_key
const API_CLIENT = env.client_id
const API_DOCS = ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest']
const API_SCOPE = 'https://www.googleapis.com/auth/calendar.readonly'

// Maximum number of calendar entries to fetch
const RESULTS = 10

// Local state
const state = {
  results: []
}

function calendar ({ auth }) {
  utils.mustbe(auth, AuthApi)

  //
  this.name = 'calendar'

  //
  this.results = () => state.results

  //
  this.fetch = (makeable) => {
    // Don't fetch if not authenticated
    if (auth.results() !== true) return Promise.resolve(state.results)

    // Minimum start time to fetch events for
    let min = new Date()

    const request = {
      calendarId: 'primary',
      timeMin: (min.setHours(0, 0, 0, 0), min.toISOString()),
      showDeleted: false,
      singleEvents: true,
      maxResults: RESULTS,
      orderBy: 'startTime'
    }

    const executor = (resolve, reject) => {
      const list = (response) => {
        let data = response.result.items
        let results = makeable ? data.map(makeable) : data

        resolve(state.results = results)
      }

      gapi.client.calendar.events.list(request).then(list)
    }

    return new Promise(executor)
  }
}

// calendar implements Api
calendar.prototype = new Api()

export default calendar
