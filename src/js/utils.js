import daysin from './utils/daysin'
import labels from './utils/labels'
import phpize from './utils/phpize'

const mustbe = (instances, of) => {
  if (!Array.isArray(instances)) instances = [instances]

  for (let i in instances) {
    let instance = instances[i]

    if (!(instance instanceof of)) {
      throw new TypeError(`'instance' must be an instance of ${of.name}`)
    }
  }
}

const isset = (accessor) => {
  try {
    return typeof accessor() !== 'undefined'
  } catch (e) {
    return false
  }
}

export default {
  phpize,
  labels,
  daysin,
  mustbe,
  isset
}
