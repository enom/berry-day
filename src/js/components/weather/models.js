// Models
import { Filter } from './interfaces'

function Forecast (data) {
  this.raw = data
  this.rain = data.pop
  this.temp = data.temp.metric
  this.feel = data.feelslike.metric
  this.time = data.FCTTIME.hour_padded
}

function All () {
  this.run = () => true
}

// All implements Filter
All.prototype = new Filter()

export {
  All,
  Forecast
}
