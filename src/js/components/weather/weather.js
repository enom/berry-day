// Component
import utils from '../../utils'
import { Filter } from './interfaces'

function Weather (filter) {
  utils.mustbe(filter, Filter)

  this.forecasts = []
  this.load = (results) => {
    this.forecasts = results.filter(filter.run)
  }
}

export default Weather
