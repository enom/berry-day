// Component
import utils from '../../utils'
import api from '../../api/api'

export const STATUS_DOWN = 'down'
export const STATUS_PENDING = 'pending'
export const STATUS_SUCCESS = 'success'
export const STATUS_FAIL = 'fail'

function Http (...apis) {
  this.last = new Date()
  this.apis = []
  this.pending = []
  this.success = []
  this.fail = []

  this.toJson = () => JSON.stringify(this)

  this.ping = () => {} // noop
  this.pong = (resolver) => {
    this.ping = resolver()
  }

  this.move = (api, into) => {
    const lists = [
      this.pending,
      this.success,
      this.fail
    ]

    // Splice api out of all statuses
    lists.forEach((list) => {
      list.forEach((item, i) => {
        if (item === api) list.splice(i, 1)
      })
    })

    // Add api to related status
    into.push(api)

    // Touch our last updated timestamp
    this.last = new Date()

    // Notify we just changed an api's status
    this.ping()
  }

  this.rig = (api) => {
    // Store original api.fetch()
    let promise = api.fetch

    // Inject an api.ping() whenever api.fetch() is called
    api.fetch = (...args) => {
      // Moves the current api() into pending and notify change
      this.move(api, this.pending)

      // Hijack api.fetch().then() to mark api as succeeded
      const resolved = (...args) => {
        this.move(api, this.success)

        return Promise.resolve(...args)
      }

      // Hijack api.fetch().catch() to mark api as failed
      const rejected = (...args) => {
        this.move(api, this.fail)

        // Pass along the original error
        // eslint-disable-next-line
        return Promise.reject(...args)
      }

      return promise(...args).then(resolved).catch(rejected)
    }

    //
    this.apis.push(api)
  }

  this.load = (apis) => {
    if (Array.isArray(apis)) {
      for (let i in apis) this.load(apis[i])
    } else {
      // All apis must implement the api interface
      utils.mustbe(apis, api)

      this.rig(apis)
    }
  }

  //
  this.load(apis)
}

export default Http
