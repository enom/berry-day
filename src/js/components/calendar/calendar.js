// Component
import utils from '../../utils'
import { Filter } from './interfaces'
import { Day } from './models'

function Calendar (filter) {
  utils.mustbe(filter, Filter)
  utils.mustbe(filter, Day)

  this.date = filter.from
  this.events = []
  this.load = (results) => {
    this.events = results.filter(filter.run)
  }
}

export default Calendar
