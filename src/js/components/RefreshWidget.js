// eslint-disable-next-line
import { h } from 'hyperapp'

function RefreshWidget({ refresh }){
  return (
    <button id='refresh' onclick={ refresh }>Refresh</button>
  )
}

export default RefreshWidget
