// eslint-disable-next-line
import { h } from 'hyperapp'
import utils from '../utils'
import Auth from '../api/auth'


function ShowLoading({ auth }){
  return auth.results() === null ? (
    <span class='auth-loading'></span>
  ) : ''
}

function ShowLogin({ auth }){
  return auth.results() === false ? (
    <button class='auth-login' onclick={auth.login}></button>
  ) : ''
}

function ShowLogout({ auth }){
  return auth.results() === true ? (
    <button class='auth-logout' onclick={auth.logout}></button>
  ) : ''
}

function ShowStatus({ auth }){
  let label = ''
  let authed = auth.results()

  if (authed === null) label = 'loading'
  if (authed === false) label = 'guest'
  if (authed === true) label = 'authed'

  return (
    <div class='auth-status'>{label}</div>
  )
}

//
function AuthWidget({ auth }){
  utils.mustbe(auth, Auth)

  let authed = auth.results()

  return (
    <div class='auth'>
      <ShowLoading auth={auth} />
      <ShowLogin auth={auth} />
      <ShowLogout auth={auth} />
      <ShowStatus auth={auth} />
    </div>
  )
}

export default AuthWidget
