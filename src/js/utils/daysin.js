const daysin = {
  // DOES NOT ACCOUNT FOR LEAP YEARS
  january: 31,
  february: 28,
  march: 31,
  april: 30,
  may: 31,
  june: 30,
  july: 31,
  august: 31,
  september: 30,
  october: 31,
  november: 30,
  december: 31,
  year: 365,
  months: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
  to: {
    days: (date) => {
      const sum = (a, b) => a + b

      let m = date.getMonth()
      let d = date.getDate()

      return daysin.months.slice(0, m).reduce(sum, -1) + d
    },
    weeks: (date) => {
      return Math.floor(daysin.to.days(date) / 7)
    }
  }
}

export default daysin
