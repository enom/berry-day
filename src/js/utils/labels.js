export default {
  day: (date) => ([
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'][date.getDay()]),
  month: (date) => ([
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'][date.getMonth()]),
  suffix: (date) => ([
    '',
    'st',
    'nd',
    'rd',
    'th'
  ][date.getDate() > 3 ? 4 : date.getDate()])
}
